# Microservices Workshop
Lab 01: Setting up the required infrastructure with Docker

---

## Preparation

 - Ask the instructor for the server IP and credentials

## Tasks

 - Deploy the required infrastructure using docker-compose (jenkins/gitlab)
 - Setup Jenkins master
 - Add a slave on the infrastructure server for CI pipelines
 - Add a slave on the Test environment server for CD pipelines
 - Add a slave on the Production environment server for CD pipelines
 - Setup Gitlab server
 - Import application repositories

## Instructions

 - Access to your infrastructure server
 
 - Clone the "Infrastructure" repository:

```
$ git clone https://oauth2:ybhqWsc_mnXujKnCnmLK@gitlab.com/sela-microservices-workshop-os/infrastructure.git ~/infrastructure
$ cd ~/infrastructure
```

 - Edit the .env file and set your server ip:

```
$ vim .env
```
```
SERVER_IP=<your-server-ip>
```
 
 - Use docker compose to set up the environment tools (gitlab and jenkins):

```
$ docker-compose up -d
```

 - The Gitlab container while take a while to get ready, you can use docker logs command in a new terminal session to track the process:

```
$ docker logs --follow gitlab
```



### Jenkins Configuration (Infrastructure Server)

 - Browse to the jenkins portal:

```
http://<ip>:8080
```

 - Unlock jenkins using the administrator password, use the command below to retrieve it:

```
$ docker exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword
```

![Image 1](Images/lab01-jenkins-01.png)

 - Select "Install suggested plugins" and wait for the plugins to being installed: 

![Image 2](Images/lab01-jenkins-02.png)

 - If you get installation failures, click retry (Email Extension plugin is not required)

 - You will be asked for credentials, set the details below:

```
Username: sela
Password: sela
FullName: sela
Email: sela@sela.com
```

<img alt="Image 3" src="Images/lab01-jenkins-03.png"  width="75%" height="75%">

 - Set "http://{infrastructure-server-ip}:8080" as the Jenkins URL
  
 - Note: You can update the user details in "Manage Jenkins"/"Manage Users"/"settings"

<img alt="Image 4" src="Images/lab01-jenkins-04.png"  width="75%" height="75%">
 
 - Once finished, click on "Restart" to restart jenkins and apply the configurations


### Jenkins Slave Configuration (Infrastructure Server)

 - Create a new folder to be used for the Jenkins slave:

```
$ sudo mkdir /home/jenkins
$ sudo chmod 777 /home/jenkins
```

 - Configure the server as a jenkins slave, start creating a new node in the jenkins portal:

```
"Manage Jenkins" / "Manage Nodes" / "New Node"
Set "Slave" as name and select "Permanent Agent"
```
 
![Image 5](Images/lab01-slave-01.png)
 
 - Configure the slave with the details below:

```
number of executors: 5
Remote root directory: /home/jenkins
Labels: Slave
Usage: Use this node as much as possible
Launch method: Launch agent agents via SSH
Host: <infrastructure-server-ip>
Credentials: <create a new username/password credentials>
Host Key Verification Strategy: Non verifying Verification Strategy
```

 - Click "Save" and wait a minute to ensure that the slave is up and running (click "refresh status")

 - Back to the terminal and exit from the server (to avoid mistakes in next steps)

```
$ exit
```


### Configure Test Environment (Jenkins Slave)

 - Connect to the test environment server:

```
$ ssh sela@<test-environment-server-ip>
```

 - Create a new folder to be used for the Jenkins slave:

```
$ sudo mkdir /home/jenkins
$ sudo chmod 777 /home/jenkins
```

 - Configure the server as a jenkins slave, start creating a new node in the jenkins portal:

```
"Manage Jenkins" / "Manage Nodes" / "New Node"
Set "Test" as name and select "Permanent Agent"
```
 
 - Configure the slave with the details below:

```
number of executors: 1
Remote root directory: /home/jenkins
Labels: Test
Usage: Use this node as much as possible
Launch method: Launch agent agents via SSH
Host: <test-environment-server-ip>
Credentials: <credentials created previously>
Host Key Verification Strategy: Non verifying Verification Strategy
```

 - Click "Save" and wait a minute to ensure that the slave is up and running (click "refresh status")
 
 - Back to the terminal and exit from the server (to avoid mistakes in next steps)

```
$ exit
```


### Configure Production Environment (Jenkins Slave)

 - Connect to the test environment server:

```
$ ssh sela@<production-environment-server-ip>
```

 - Create a new folder to be used for the Jenkins slave:

```
$ sudo mkdir /home/jenkins
$ sudo chmod 777 /home/jenkins
```

 - Configure the server as a jenkins slave, start creating a new node in the jenkins portal:

```
"Manage Jenkins" / "Manage Nodes" / "New Node"
Set "Production" as name and select "Permanent Agent"
```
 
 - Configure the slave with the details below:

```
number of executors: 1
Remote root directory: /home/jenkins
Labels: Production
Usage: Use this node as much as possible
Launch method: Launch agent agents via SSH
Host: <production-environment-server-ip>
Credentials: <credentials created previously>
Host Key Verification Strategy: Non verifying Verification Strategy
```

 - Click "Save" and wait a minute to ensure that the slave is up and running (click "refresh status")
 
 - Back to the terminal and exit from the server (to avoid mistakes in next steps)

```
$ exit
```

### GitLab Configuration

 - Browse to the gitlab portal:

```
http://<infrastructure-server-ip>
```

 - The first time you will asked for set the admin password, set the password below:

```
selasela
```

![Image 8](Images/lab01-gitlab-01.png)

 - Then, to login use the following credentials:

```
Username: root
Password: selasela
```

![Image 9](Images/lab01-gitlab-02.png)


### Import Project Repositories

 - Click "New Project":

<img alt="Image 10" src="Images/lab01-import-01.png"  width="75%" height="75%">

 - Select "Import Repository" and "Repo by Url":

<img alt="Image 11" src="Images/lab01-import-02.png"  width="75%" height="75%">

 - Set the repository url and the project name:

```
Repo Url: https://github.com/selaworkshops/ui-service.git
Name: ui-service
```

<img alt="Image 12" src="Images/lab01-import-03.png"  width="75%" height="75%">


 - Set the repository as public and click "create project":


<img alt="Image 13" src="Images/lab01-import-04.png"  width="75%" height="75%">


 - Repeat the process for the repositories below:

```
https://github.com/selaworkshops/sum-service.git
sum-service
```
```
https://github.com/selaworkshops/subtraction-service.git
subtraction-service
```
```
https://github.com/selaworkshops/multiplication-service.git
multiplication-service
```
```
https://github.com/selaworkshops/division-service.git
division-service
```
```
https://github.com/selaworkshops/microservices-calculator-app.git
microservices-calculator-app
```


### Configure your docker hub account

 - Create the following repositories in your docker hub account:

```
<your-user>/ui-service
<your-user>/sum-service
<your-user>/subtraction-service
<your-user>/multiplication-service
<your-user>/division-service
```
